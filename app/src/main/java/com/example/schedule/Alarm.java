package com.example.schedule;

import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Alarm extends Service {
    private static final int NOTIF_ID = 1;
    private static final String NOTIF_CHANNEL_ID = "Channel_Id";
    private FirebaseAuth mAuth;
    String TAG = "ScheduleNotify";
    FirebaseUser currentUser;
    String display;
    Uri uri = Uri.parse("../res/raw/alarm.m4a");
    NotificationCompat.Builder mon = new NotificationCompat.Builder(this,
            NOTIF_CHANNEL_ID)
            .setOngoing(true)
            .setSmallIcon(R.drawable.ic_access_time_black_24dp);
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    PendingIntent pendingIntent;
        // 0 - for private mode
    //SharedPreferences.Editor editor = pref.edit();


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }



    public static boolean compareDates(Date date1, Date date2)
    {
        if(date1.after(date2)){
            System.out.println("Date1 is after Date2");
            return false;
        }

        if(date1.before(date2)){
            System.out.println("Date1 is before Date2");
            return false;
        }

        if(date1.equals(date2)){
            System.out.println("Date1 is equal Date2");
            return true;
        }
        return false;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId){

        pref = getSharedPreferences("my prefa", Context.MODE_PRIVATE);
        editor = pref.edit();
        startForeground();

        return super.onStartCommand(intent, flags, startId);
    }

    private void startForeground() {

        Intent notificationIntent = new Intent(this, MainActivity.class);

        pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);
        mAuth = FirebaseAuth.getInstance();

        currentUser = mAuth.getCurrentUser();

        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        Timer timer = new Timer();
        //Set the schedule function
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
              // Magic here
                stopService();
                startService();
                if(currentUser != null){
                    db.collection("Schedules")
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        if (document.get("Owner")
                                                .toString().equals(currentUser.getUid())){
                                            String date = document.get("Schedule Date")
                                                    .toString()
                                                    +" "+
                                                    document.get("Schedule Time").toString();
                                            SimpleDateFormat sdf = new SimpleDateFormat(
                                                  "dd/MM/yyyy hh:mm a");
                                            sdf.setLenient(true);
                                            try {
                                                sdf.parse(date.trim());
                                            } catch (ParseException pe) {
                                                System.out.println(pe);
                                            }
                                            Timestamp timestamp = new Timestamp(System
                                                    .currentTimeMillis());
                                            System.out.println(sdf.format(timestamp));
                                            String stamp = sdf.format(timestamp);
                                            Date pickeddate = new Date();
                                            Date currentdate = new Date();
                                            try {
                                                pickeddate = sdf.parse(date.trim());
                                                currentdate = sdf.parse(stamp.trim());
                                            } catch (ParseException ex) {
                                                System.out.println(ex);
                                            }
                                            boolean active = compareDates(pickeddate, currentdate);
                                            Log.d(TAG, document.getId() + " => "
                                                  + document.getData());
                                            if(active){
                                                display = document.get("Schedule")
                                                        .toString() + "\n"  +
                                                        document.get("Schedule Description")
                                                                .toString()
                                                        + " \n " + " is happening";
                                                editor.putString("display",display);
                                                System.out.println("true");
                                                editor.commit();
                                            }
                                        }
                                    }
                                } else {
                                Log.w(TAG, "Error getting your schedules.",
                                      task.getException());
                                }
                            }
                        });
                    }
                }
          },
        60000, 600000000);
        Notify();
    }

    public void Notify(){
        display = pref.getString("display",null);
        if(display != null){
            System.out.println(display);
            startForeground(NOTIF_ID,
                    mon.setContentTitle(getString(R.string.app_name))
                            .setContentText(display)
                            .setSound(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                                    + "://" + this.getBaseContext().getPackageName()+"/"+R.raw.alarm))
                            .setContentIntent(pendingIntent)
                            .setVibrate(new long[] {5000, 5000, 5000, 5000})
                            .setColorized(true)
                            .setColor(0xFFFFFFFF)
                            .build());
            editor.putString("display",null);
        }
        editor.clear();
        editor.commit();
    }

    public void stopService(){
        stopService(new Intent(this, Alarm.class));
    }

    public void startService(){
        startService(new Intent(this, Alarm.class));
    }
}
