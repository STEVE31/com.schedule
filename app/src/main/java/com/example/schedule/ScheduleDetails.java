package com.example.schedule;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class ScheduleDetails extends AppCompatActivity implements View.OnClickListener {

    Button backbutton;
    TextView schedName;
    TextView textDesc;
    TextView textDte;
    TextView textTime;
    private FirebaseAuth mAuth;

    @Override
    protected void onStart() {
        super.onStart();
        mAuth = FirebaseAuth.getInstance();
        final FirebaseUser currentUser = mAuth.getCurrentUser();
        final FirebaseFirestore db = FirebaseFirestore.getInstance();


        schedName = findViewById(R.id.schedName);
        textTime = findViewById(R.id.textTime);
        textDte = findViewById(R.id.textDte);
        textDesc = findViewById(R.id.textDesc);

        schedName.setText(getIntent().getStringExtra("text"));
        final String sched = getIntent().getStringExtra("text");

        db.collection("Schedules")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if (document.get("Owner").toString().equals(currentUser.getUid())) {
                                    if(document.get("Schedule").toString().equals(sched)){
                                        textDesc.setText(document.get("Schedule Description").toString());
                                        textDte.setText(document.get("Schedule Date").toString());
                                        textTime.setText(document.get("Schedule Time").toString() );
                                    }
                                }
                            }
                        }else {
                        Log.w("My Content", "Error getting your schedules.", task.getException());
                    }
                }
    });



}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_details);

        backbutton = findViewById(R.id.buttonback);
        backbutton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, Navigator.class);
        startActivity(intent);
    }
}
