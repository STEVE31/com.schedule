package com.example.schedule;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Profile extends Fragment implements View.OnClickListener {

    private Button buttonback;
    private FirebaseAuth mAuth;
    private TextView emailView;
    String TAG = "LogOut";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView (@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        buttonback = (Button) view.findViewById(R.id.buttonback);
        emailView = (TextView) view.findViewById(R.id.emailView);
        Log.d("meee","mee");
        buttonback.setOnClickListener(this);
        FirebaseUser currentUser = mAuth.getCurrentUser();
        emailView.setText(currentUser.getEmail());
    }

    @Override
    public void onClick(View view) {
        Log.d("Hoooooooooof", "hoof");
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            Toast.makeText(this.getActivity(), currentUser.getEmail()
                    + " has logged out", Toast.LENGTH_SHORT).show();
            mAuth.signOut();
        }
        Intent intent = new Intent(this.getContext(), MainActivity.class);
        startActivity(intent);
        startActivity(new Intent(getActivity(), MainActivity.class));
        Log.d("Hoooooooooof", "hoof");
    }
}
