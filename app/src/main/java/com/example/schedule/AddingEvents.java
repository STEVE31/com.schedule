package com.example.schedule;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class AddingEvents extends Fragment {

    private FirebaseAuth mAuth;
    private EditText datePicker;
    private final Calendar myCalendar = Calendar.getInstance();
    private final Calendar meCalendar = Calendar.getInstance();
    private EditText timePicker;
    private EditText editScheduleName;
    private EditText editDescription;
    private Button buttonSetSchedule;
    private String TAG = "AddingEventsActivity";

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mAuth = FirebaseAuth.getInstance();
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_adding_events, container, false);
    }

    private static boolean compareDates(Date date1, Date date2)
    {
        if(date1.after(date2)){
            System.out.println("Date1 is after Date2");
            return true;
        }

        if(date1.before(date2)){
            System.out.println("Date1 is before Date2");
            return false;
        }

        if(date1.equals(date2)){
            System.out.println("Date1 is equal Date2");
            return false;
        }
        return false;
    }

    private void updateTime() {
        String myFormat = "hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        timePicker.setText(sdf.format(meCalendar.getTime()));
    }

    @SuppressLint("ClickableViewAccessibility")
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        mAuth = FirebaseAuth.getInstance();

        final FirebaseUser currentUser = mAuth.getCurrentUser();
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        datePicker = view.findViewById(R.id.datePicker);
        timePicker = view.findViewById(R.id.timePicker);
        editScheduleName = view.findViewById(R.id.editScheduleName);
        editDescription = view.findViewById(R.id.editDescription);
        buttonSetSchedule = view.findViewById(R.id.buttonSetSchedule);

        buttonSetSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editScheduleName.getText().toString().isEmpty()){

                }
                if(editScheduleName.getText().toString().equals("")){
                    editScheduleName.requestFocus();
                    editScheduleName.setError("Input Schedule Name");
                   // Toast.makeText(getContext(), "Input Schedule Name", Toast.LENGTH_SHORT)
                   //         .show();
                }else if(datePicker.getText().toString().equals("")){
                    //datePicker.requestFocus();
                    datePicker.setError("Set Schedule Date");
                   // Toast.makeText(getContext(), "Set Schedule Date", Toast.LENGTH_SHORT)
                   //         .show();
                }else if(timePicker.getText().toString().equals("")){
                    //timePicker.requestFocus();
                    timePicker.setError("Set Schedule Time");
                    Toast.makeText(getContext(), "Set Schedule Time", Toast.LENGTH_SHORT)
                            .show();
                }else if(editDescription.getText().toString().equals("")){
                    editDescription.requestFocus();
                    editDescription.setError("Input Schedule description");
                    Toast.makeText(getContext(), "Input Schedule description",
                            Toast.LENGTH_SHORT).show();
                }else{
                    String date =  datePicker.getText().toString() + " " + timePicker.getText().toString();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
                    sdf.setLenient(true);
                    try {
                        sdf.parse(date.trim());
                        System.out.println("date");
                    } catch (ParseException pe) {
                        System.out.println(pe);
                    }
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    System.out.println(sdf.format(timestamp));
                    String stamp = sdf.format(timestamp);
                    Date pickeddate = new Date();
                    Date currentdate = new Date();
                    try {
                        pickeddate = sdf.parse(date.trim());
                        currentdate = sdf.parse(stamp.trim());
                    } catch (ParseException ex) {
                        System.out.println(ex);
                    }
                    System.out.println(sdf.format(pickeddate));
                    boolean active = compareDates(pickeddate, currentdate);
                    if(active){
                        // Create a new user with a first and last name
                        Snackbar.make(getView(), "Setting Schedule", Snackbar.LENGTH_SHORT)
                                .setAction("Action",null).show();
                        Map<String, Object> Schedule = new HashMap<>();
                        Schedule.put("Owner", currentUser.getUid());
                        Schedule.put("Schedule", editScheduleName.getText().toString());
                        Schedule.put("Schedule Date", datePicker.getText().toString());
                        Schedule.put("Schedule Time", timePicker.getText().toString());
                        Schedule.put("Schedule Description", editDescription.getText().toString());

                        // Add a new document with a generated ID
                        db.collection("Schedules")
                                .add(Schedule)
                                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                    @Override
                                    public void onSuccess(DocumentReference documentReference) {
                                        Log.d(TAG, "DocumentSnapshot added with ID: "
                                                + documentReference.getId());
                                        Snackbar.make(getView(), "Schedule Set", Snackbar.LENGTH_SHORT)
                                                .setAction("Action",null).show();
                                    /*Toast.makeText(getContext(), "Schedule Set",
                                            Toast.LENGTH_SHORT).show();*/
                                        editScheduleName.setText("");
                                        datePicker.setText("");
                                        timePicker.setText("");
                                        editDescription.setText("");
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.w(TAG, "Error adding document", e);
                                        Toast.makeText(getContext(), "Schedule was not set",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }else{
                        Toast.makeText(getContext(), "Pick a date that is after this minute",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDate();
                Log.d("date set", myCalendar.getTime().toString());
            }

        };

        final TimePickerDialog.OnTimeSetListener time = new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int hour, int minute) {
                meCalendar.set(Calendar.HOUR, hour);
                meCalendar.set(Calendar.MINUTE, minute);
                updateTime();
                Log.d("time set", meCalendar.getTime().toString());
            }
        };

        datePicker.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    DatePickerDialog fin = new DatePickerDialog(getContext(), date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH));
                    fin.setCanceledOnTouchOutside(true);
                    fin.show();
                    updateDate();
                    return true;
                }
                return false;
            }
        });

        timePicker.setOnTouchListener(new View.OnTouchListener() {
              @Override
              public boolean onTouch(View view, MotionEvent motionEvent) {
                  if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                      TimePickerDialog fin = new TimePickerDialog(getContext(), time, meCalendar
                              .get(Calendar.HOUR), meCalendar.get(Calendar.MINUTE),
                              false);
                      fin.setCanceledOnTouchOutside(true);
                      fin.show();
                      Log.d("setting", "time");
                      updateTime();
                      return true;
                  }
                  return false;
              }
          });

    }

    private void updateDate() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        datePicker.setText(sdf.format(myCalendar.getTime()));
    }

}

