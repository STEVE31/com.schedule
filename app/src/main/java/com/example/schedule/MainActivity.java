package com.example.schedule;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private FirebaseAuth mAuth;
    MaterialButton buttonLogin;
    TextView textToSignUp;
    EditText editEmail;
    EditText editPassword;
    String TAG = "MainActivity: ";

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            Intent intent = new Intent(this, Navigator.class);
            startActivity(intent);
            Toast.makeText(this, currentUser.getEmail(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        buttonLogin = findViewById(R.id.buttonLogin);
        textToSignUp = findViewById(R.id.textToSignUp);
        editEmail = findViewById(R.id.editEmail);
        editPassword = findViewById(R.id.editPassword);

        buttonLogin.setOnClickListener(this);
        textToSignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonLogin:
                final String email = editEmail.getText().toString();
                final String password = editPassword.getText().toString();
                isValid(email);
                final Intent intent = new Intent(this, Navigator.class);
                if (email.equals("") || password.equals("")){
                    Toast.makeText(this,"please input email and password",
                            Toast.LENGTH_LONG).show();
                }else if (password.length() < 5 ){
                    Toast.makeText(this,
                            "please input a password of up to 5 characters",
                            Toast.LENGTH_LONG).show();
                }else if(!isValid(email)){
                    Toast.makeText(this,
                            "please input a valid email",
                            Toast.LENGTH_LONG).show();
                }else    {
                    Snackbar.make(view, "Logging in...",
                            Snackbar.LENGTH_LONG)
                            .setAction("Action",null)
                            .show();
                    mAuth.signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        Log.d(TAG, "signInWithEmail:success");
                                        FirebaseUser user = mAuth.getCurrentUser();
                                        if(user.isEmailVerified()){
                                            Toast.makeText(MainActivity.this,
                                                    user.getEmail() + " has logged in",
                                                    Toast.LENGTH_SHORT).show();
                                            startActivity(intent);
                                        }else{
                                            sendVerifEmail(user);
                                            mAuth.signOut();
                                            Toast.makeText(MainActivity.this,
                                                    user.getEmail()
                                                            + " verification email has been resent",
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                        Log.d("mail & password output",email + password);
                                    } else {
                                        // If sign in fails, display a message to the user.
                                        if(task.getException().toString()
                                                .contains("The user may have been deleted")){
                                            Toast.makeText(MainActivity.this,
                                                    "The user does not exist.",
                                                    Toast.LENGTH_SHORT).show();
                                        }else if (task.getException().toString()
                                                .contains("The password is invalid")){
                                            Toast.makeText(MainActivity.this,
                                                    "Wrong password.",
                                                    Toast.LENGTH_SHORT).show();
                                        }else{
                                            Log.w(TAG, "signInWithEmail:failure",
                                                    task.getException());
                                            Toast.makeText(MainActivity.this,
                                                    "Authentication failed.",
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                }
                            });

                }
                break;
            case R.id.textToSignUp:
                Intent intent2 = new Intent(this, SchedulerRegister.class);
                startActivity(intent2);
                break;
        }
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_LONG).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    static boolean isValid(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    public void sendVerifEmail(FirebaseUser user){
        user = mAuth.getCurrentUser();
        user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()) {
                    Log.d(TAG,"Email resent");
                }
            }
        });
    }
}
