package com.example.schedule;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SchedulerRegister extends AppCompatActivity implements View.OnClickListener {

    TextView textToLogin;
    Button buttonRegister;
    private FirebaseAuth mAuth;
    EditText editEmail;
    EditText editPassword;
    String TAG = "MyRegisterActivity";
    private ProgressDialog progress;
    FirebaseUser user;

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            Intent intent = new Intent(this, Navigator.class);
            startActivity(intent);
            Toast.makeText(this, currentUser.getEmail(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scheduler_register);

        mAuth = FirebaseAuth.getInstance();

        buttonRegister = findViewById(R.id.buttonRegister);
        textToLogin = findViewById(R.id.textToLogin);
        editEmail = findViewById(R.id.editEmail);
        editPassword = findViewById(R.id.editPassword);

        buttonRegister.setOnClickListener(this);
        textToLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonRegister:
                String email = editEmail.getText().toString();
                final String password = editPassword.getText().toString();
                isValid(email);
                if (email.equals("") || password.equals("")){
                    Toast.makeText(this,"please input email and password",
                            Toast.LENGTH_LONG).show();
                }else if (password.length() < 5 ){
                    Toast.makeText(this,
                            "please input a password of up to 5 characters",
                            Toast.LENGTH_LONG).show();
                }else if(!isValid(email)){
                    Toast.makeText(this,"please input a valid email",
                            Toast.LENGTH_LONG).show();
                }else    {
                    Log.d("mail & password output",email + password);
                    progressbar();
                    final Intent intent = new Intent(this, MainActivity.class);
                    mAuth.createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        Log.d(TAG, "createUserWithEmail:success");
                                        user = mAuth.getCurrentUser();
                                        updateUI(user);
                                        sendEmail(user);
                                        Toast.makeText(SchedulerRegister.this,
                                                user.getEmail(),
                                                Toast.LENGTH_LONG).show();
                                        mAuth.signOut();
                                        startActivity(intent);
                                    } else if (task.getException()
                                            .toString()
                                            .contains("The email address is already in use by another account.")) {
                                        // If sign in fails, display a message to the user.
                                        progress.cancel();
                                        Log.w(TAG, "createUserWithEmail:failure",
                                                task.getException());
                                        Toast.makeText(SchedulerRegister.this,
                                                "The email address is already in use by another account.",
                                                Toast.LENGTH_LONG).show();
                                        updateUI(null);
                                    }else{
                                        progress.cancel();
                                        Toast.makeText(SchedulerRegister.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }


                break;
            case R.id.textToLogin:
                Intent intent2 = new Intent(this, MainActivity.class);
                startActivity(intent2);
                break;
        }
    }

    static boolean isValid(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            buttonRegister.setVisibility(View.GONE);
        } else {
            buttonRegister.setVisibility(View.VISIBLE);
        }
    }

    public void progressbar(){
        progress=new ProgressDialog(this);
        progress.setMessage("Registering User");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.show();

        final int totalProgressTime = 100;
        final Thread t = new Thread() {
            @Override
            public void run() {
                int jumpTime = 0;

                while(jumpTime < totalProgressTime) {
                    try {
                        sleep(200);
                        jumpTime += 5;
                        progress.setProgress(jumpTime);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        };
        t.start();
    }

    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_LONG).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    public void sendEmail(FirebaseUser user){
        user = mAuth.getCurrentUser();
        user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()) {
                    Toast.makeText(SchedulerRegister.this,
                            "Email has been sent for verification",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}

/*
private void updateUI(FirebaseUser user) {
        hideProgressDialog();
        if (user != null) {
            //getting user that has signed in
            mStatusTextView.setText(getString(R.string.google_status_fmt, user.getEmail()));
            mDetailTextView.setText(getString(R.string.firebase_status_fmt, user.getUid()));

            findViewById(R.id.sign_in_button).setVisibility(View.GONE);
            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.VISIBLE);
        } else {
            mStatusTextView.setText(R.string.signed_out);
            mDetailTextView.setText(null);

            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.GONE);
        }
    }
* */