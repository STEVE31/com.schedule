package com.example.schedule;

import android.app.Application;
import android.content.Intent;

public class Alarming extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        startService(new Intent(this, Alarm.class));
    }
}
