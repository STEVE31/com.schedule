package com.example.schedule;

/*
* File to help create the recycler view items to be viewed
* Should help with the iteration
* */

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{

    private ArrayList<String> mScheduleName;
    private ArrayList<String> mScheduleImages;
    private Context mContext;

    private final String TAG = "RecyclerViewAdapter";


    public RecyclerViewAdapter(Context context,
                               ArrayList<String> scheduleNames,
                               ArrayList<String> scheduleImages) {
        mScheduleName = scheduleNames;
        mScheduleImages = scheduleImages;
        mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.each_element, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Log.d(TAG, "OnBindViewHolder: called.");

        Glide.with(mContext)
                .load(mScheduleImages.get(position))
                .asBitmap()
                .into(holder.scheduleImage);

        holder.scheduleName.setText(mScheduleName.get(position));
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Onclicked: clicked on:" + mScheduleImages.get(position));
                Toast.makeText(mContext, mScheduleName.get(position), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(mContext, ScheduleDetails.class);
                intent.putExtra("text", mScheduleName.get(position));
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mScheduleImages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CircleImageView scheduleImage;
        TextView scheduleName;
        LinearLayout parentLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            scheduleImage = itemView.findViewById(R.id.schedule_image);
            scheduleName = itemView.findViewById(R.id.schedule_name);
            parentLayout = itemView.findViewById(R.id.parent_layout);

        }
    }

}
