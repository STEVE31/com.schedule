package com.example.schedule;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class Schedules extends Fragment {


    private static final String TAG = "AddingEvents";

    private ArrayList<String> mScheduleNames = new ArrayList<>();
    private ArrayList<String> mScheduleImageUrls = new ArrayList<>();
    private FirebaseAuth mAuth;
    private int i;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mAuth = FirebaseAuth.getInstance();
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_schedules, container, false);
    }

    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        Snackbar.make(view, "About to display your schedules ...", Snackbar.LENGTH_SHORT)
                .setAction("Action",null)
                .show();

        final FirebaseUser currentUser = mAuth.getCurrentUser();
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        RecyclerView recyclerView = view.findViewById(R.id.recyclerviews);
        final RecyclerViewAdapter adapter = new RecyclerViewAdapter(this.getContext(), mScheduleNames, mScheduleImageUrls);
        //adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));

        i = 0;
        db.collection("Schedules")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if (document.get("Owner").toString().equals(currentUser.getUid())){
                                    mScheduleNames.add(0, document.get("Schedule").toString());
                                    mScheduleImageUrls.add(0, "https://www.crockerriverside.org/sites/main/files/imagecache/square/main-images/camera_lense_0.jpeg");
                                    i++;
                                    Log.d(TAG, document.getId() + " => " + document.getData());
                                }
                            }
                            if(i == 0){
                                Toast.makeText(getContext(),
                                        "You do not have any existing schedules. \r\n " +
                                        "Click on add schedule at the bottom", Toast.LENGTH_LONG)
                                        .show();
                            }else{
                                //Toast.makeText(getContext(), "About to display your schedules ...", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getContext(), "Error getting your schedules.", Toast.LENGTH_SHORT).show();
                            Log.w(TAG, "Error getting your schedules.", task.getException());
                        }
                        adapter.notifyDataSetChanged();
                    }
                });



        /*
        mScheduleNames.add("Steve");
        mScheduleImageUrls.add("https://cdn.arstechnica.net/wp-content/uploads/2016/02/5718897981_10faa45ac3_b-640x624.jpg");

        mScheduleNames.add("Bella");
        mScheduleImageUrls.add("https://www.crockerriverside.org/sites/main/files/imagecache/square/main-images/camera_lense_0.jpeg");

        mScheduleNames.add("Carre");
        mScheduleImageUrls.add("https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/Ultraviolet_image_of_the_Cygnus_Loop_Nebula_crop.jpg/691px-Ultraviolet_image_of_the_Cygnus_Loop_Nebula_crop.jpg");
        */
    }


}
